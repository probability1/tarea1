#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Tarea 1
"""

""" 
Autor: Jorge Muñoz Taylor
Carné: A53863
Curso: Modelos probabilísticos de señales y sistemas para ingeniería
Grupo: 01
Fecha: 8/05/2020
"""

import sys 
import os #Contiene las funciones que permiten acceder a operaciones básicas del sistema operativo, en este caso, la búsqueda de un archivo en un path especificado.
import csv #Permite manipular archivos CSV
from pathlib import Path #Contiene la función path.isfile que permite verificar si existe un archivo en la dirección especificada.


"""
Función que se encarga de determinar si el path dado por el usuario al momento de ejecutar el programa existe,
primero revisa que se haya introducido únicamente un argumento en la terminal -en caso contrario regresa False-
, luego busca el archivo en la dirección proveída -en caso contrario regresa False-.

Parámetros de la función:

in [archivo]: Dirección del archivo CSV con los datos necesarios para los cálculos.
out [False / True]: Si el archivo está en la dirección dada regresa True, en caso contrario False.
"""
def verificar_archivo(archivo):

    #Determina si el array archivo contiene únicamente dos elementos.
    if len(archivo) == 2:
    
        #Busca el archivo CSV en la dirección dada por el usuario -la dirección está almacenada en la posición 1 del arrar "archivo"-.
        if os.path.isfile (archivo[1]):
            print ("\n-> El archivo existe")
            return True
        else:
            print ("\n-> El archivo no existe...")
            return False

    else:
        print ("\n-> Introduzca únicamente la dirección del archivo CSV :/")
        return False



"""
Está función se encarga de sacar la info del documento CSV y colocarlos en un diccionario,
este diccionario es regresado al lazo de control principal del programa.

Parámetros de la función:

in [direccion_archivo]: Dirección del archivo CSV con los datos necesarios para los cálculos.
out [datos]: Diccionario con los datos incluidos en el archivo CSV.
"""
def abrir_csv (direccion_archivo):

    # Array vacío, posteriormente se llenará con los datos extraídos del CSV.
    datos = []

    """
    Se abre el archivo y se etiqueta como FILE, su información se almacena en la variable informacion_File,
    luego se accesa a cada línea de datos a través de un bucle for y cada una de esas líneas se almacena
    en el array "datos" creado con anterioridad.
    """
    with open(direccion_archivo) as File:  
        informacion_File = csv.DictReader(File)
        for fila in informacion_File:
            datos.append(fila)

    # Regresa el array al lazo principal del programa.
    return datos



"""
Función que encuentra la probabilidad de ocurrencia de una característica durante un experimento, primero detecta si la
característica ingresada existe -regresa -1 si no es así-, luego por medio de un bucle for cuenta el número de veces que 
se da dicha característica durante el experimento y lo divide entre la cantidad de repeticiones que se hicieron en el
experimento. 

Parámetros de la función:

in [N]: Cantidad de repeticiones del experimento.
in [CARACTERISTICA]: La característica del experimento que se desea usar.
in [DATOS]: El diccionario con los datos del experimento.
out [resultado/-1]: Regresa la probabilidad de ocurrencia de esa característica durante el experimento, 
                    en caso de que la característica no exista regresa -1.
"""
def ocurrencia_de_caracteristica (N, CARACTERISTICA, DATOS):

    # Contador, cada vez que se encuentre que la característica ocurrió se suma 1.
    ocurrio_caracteristica = 0

    # Se pregunta si la característica existe.
    if CARACTERISTICA == 'A':

        """
        Recorre el array DATOS para buscar en cada diccionario (cada línea del array es un diccionario) si ocurrió o no
        la característica.
        """
        for FILA in DATOS:        
            if int(FILA['A']) == 1:
                ocurrio_caracteristica += 1
        
        # Se toma la totalidad de veces que ocurrió la característica y se divide entre N, luego se regresa al lazo principal.
        resultado = ocurrio_caracteristica/N
        return resultado

    
    elif CARACTERISTICA == 'B': 
        for FILA in DATOS:        
            if int(FILA['B']) == 1:
                ocurrio_caracteristica += 1

        resultado = ocurrio_caracteristica/N
        return resultado


    elif CARACTERISTICA == 'C':
        for FILA in DATOS:        
            if int(FILA['C']) == 1:
                ocurrio_caracteristica += 1

        resultado = ocurrio_caracteristica/N
        return resultado


    elif CARACTERISTICA == 'D':
        for FILA in DATOS:        
            if int(FILA['D']) == 1:
                ocurrio_caracteristica += 1

        resultado = ocurrio_caracteristica/N
        return resultado


    else:
        print ("\n-> Esa característica no existe")
        return -1



"""
Función que determina si las características X e Y tienen relación de dependencia, independencia o si son
mutuamente excluyentes. Esto lo hace verificando tres escenarios: 
1. Si la probabilidad conjunta de X e Y es igual a P(X)*P(Y), en este caso X e Y son independientes.
2. Si la probabilidad conjunta de X e Y es mayor a 0, en este caso X e Y son dependientes. 
3. Si la probabilidad conjunta de X e Y es igual a 0, en este caso son mutuamente excluyentes.

Parámetros de la función:

in [P_XnY]: Probabilidad conjunta de los elementos X e Y. 
in [PX]: Probabilidad de ocurrencia de X.
in [PY]: Probabilidad de ocurrencia de Y.
in [X]: Característica X.
in [Y]: Característica Y.
"""
def relacion_dep_indep (P_XnY, PX, PY, X, Y):

    PXY   = PX*PY
    PXY5  = PXY + PXY*0.1
    PXY_5 = PXY - PXY*0.1
    

    if P_XnY == PXY:
        print ("\n\t-> " +X+ " y " +Y+ " son independientes (umbral +-10%)")

    elif P_XnY >= PXY_5 and P_XnY <= PXY5:
        print ("\n\t-> " +X+ " y " +Y+ " son independientes (umbral +-10%)")

    elif P_XnY > 0:
        print ("\n\t-> " +X+ " y " +Y+ " son dependientes (umbral +-10%)")

    elif P_XnY == 0:
        print ("\n\t-> " +X+ " y " +Y+ " son dependientes (mutuamente excluyentes)")



"""
Función que se encarga de calcular la probabilidad condicional de dos eventos usando el teorema de BAYES.

Parámetros de la función:

in [probaBA]: Probabilidad de B dado que ocurrió A.
in [probaA]: Probabilidad de A.
in [probaB]: Probabilidad de B.
out [probaAB]: Regresa la probabilidad de A dado que ocurrió B.
"""
def Bayes ( probaBA, probaA, probaB ):

    probaAB = (probaBA * probaA)/ probaB

    return probaAB




"""
Función main del programa
"""
if __name__ == "__main__":

    # Variable que almacena los argumentos introducidos por el usuario en la terminal.
    archivo = sys.argv

    """
    Variables que se usaran luego, se inicializaron por que todas ellas funcionan como contadores
    en lazos for, y para mantener el orden/legibilidad.
    """
    N   = 0 # Número de veces que se repite el evento.
    PA  = 0 # Probabilidad de que ocurra la característica A.
    PB  = 0 # Probabilidad de que ocurra la característica B.
    PC  = 0 # Probabilidad de que ocurra la característica C.
    PD  = 0 # Probabilidad de que ocurra la característica D.
    AnB = 0 # Número de veces que las características A y B ocurren simultáneamente.
    AnC = 0 # Número de veces que las características A y C ocurren simultáneamente.
    AnD = 0 # Número de veces que las características A y D ocurren simultáneamente.
    BnA = 0 # Número de veces que las características B y A ocurren simultáneamente.
    BnC = 0 # Número de veces que las características B y C ocurren simultáneamente.
    BnD = 0 # Número de veces que las características B y D ocurren simultáneamente.
    CnA = 0 # Número de veces que las características C y A ocurren simultáneamente.
    CnB = 0 # Número de veces que las características C y B ocurren simultáneamente.
    CnD = 0 # Número de veces que las características C y D ocurren simultáneamente.
    DnA = 0 # Número de veces que las características D y A ocurren simultáneamente.
    DnB = 0 # Número de veces que las características D y B ocurren simultáneamente.
    DnC = 0 # Número de veces que las características D y C ocurren simultáneamente.


    """
    Se llama a la función "verificar_archivo" con el objetivo de identificar si la dirección del
    documento CSV almacenada en la variable "archivo" existe, si es así devuelve True y se continúa
    con el programa, en caso de que regrese False se acaba el mismo.
    """
    if verificar_archivo (archivo) is True:

        """
        La funcion "abrir_csv" regresa un array de diccionarios que contiene la información del archivo
        csv, ese array se almacena en el nuevo array "DATOS".
        """
        DATOS = abrir_csv (archivo[1])

        # Contabiliza las repeticiones.
        for FILA in DATOS:
            N += 1


        """
        Parte 1: probabilidad de ocurrencia de cada característica
        """

        print ("\n1. ¿Cuál es la probabilidad de ocurrencia de cada característica?")

        """
        El cálculo de la ocurrencia de cada característica se hace en la función "ocurrencia_de_caracteristica",
        esta función regresa la probabilidad de ocurrencia de una característica deseada (A, B, C, D), en caso de 
        que se ingrese una característica que no existe (por ejemplo Z) está función regresa -1.
        """

        #Cálculo de la probabilidad de la característica A
        resultado = ocurrencia_de_caracteristica (N, 'A', DATOS)

        if resultado != -1:
            PA = resultado
            print ("\n\tProbabilidad de A: ", PA)


        #Cálculo de la probabilidad de la característica B
        resultado = ocurrencia_de_caracteristica (N, 'B', DATOS)

        if resultado != -1:
            PB = resultado
            print ("\tProbabilidad de B: ", PB)


        #Cálculo de la probabilidad de la característica C
        resultado = ocurrencia_de_caracteristica (N, 'C', DATOS)

        if resultado != -1:
            PC = resultado
            print ("\tProbabilidad de C: ", PC)


        #Cálculo de la probabilidad de la característica D
        resultado = ocurrencia_de_caracteristica (N, 'D', DATOS)

        if resultado != -1:
            PD = resultado
            print ("\tProbabilidad de D: ", PD)


        """
        Parte 2: Probabilidad de una característica dado otra
        """

        print ("\n2. En todos los pares posibles, ¿cuál es la probabilidad de una característica dado otra? Ejemplo: P(C | D)")

        """
        Recorre todas las filas de la matriz "DATOS" -donde cada fila corresponde a un diccionario- y cuenta las ocaciones en 
        que las características dadas ocurren en ese evento, esto se hace por medio de una multiplicación, donde si ambas características
        ocurren dicha multiplicación tiene como resultado 1.
        """
        for FILA in DATOS:
            
            AnB += 1 if (int(FILA['A']) * int(FILA['B']) == 1) else 0
            AnC += 1 if (int(FILA['A']) * int(FILA['C']) == 1) else 0
            AnD += 1 if (int(FILA['A']) * int(FILA['D']) == 1) else 0 

            BnA += 1 if (int(FILA['B']) * int(FILA['A']) == 1) else 0 
            BnC += 1 if (int(FILA['B']) * int(FILA['C']) == 1) else 0 
            BnD += 1 if (int(FILA['B']) * int(FILA['D']) == 1) else 0 

            CnA += 1 if (int(FILA['C']) * int(FILA['A']) == 1) else 0 
            CnB += 1 if (int(FILA['C']) * int(FILA['B']) == 1) else 0 
            CnD += 1 if (int(FILA['C']) * int(FILA['D']) == 1) else 0 

            DnA += 1 if (int(FILA['D']) * int(FILA['A']) == 1) else 0 
            DnB += 1 if (int(FILA['D']) * int(FILA['B']) == 1) else 0 
            DnC += 1 if (int(FILA['D']) * int(FILA['C']) == 1) else 0 


        """
        Cálculo de las probabilidades conjuntas, toma el número de veces que ocurren las características y lo divide entre
        la totalidad de repeticiones que tuvo el experimento.
        """
        P_AnB = AnB / N
        P_AnC = AnC / N 
        P_AnD = AnD / N
        P_BnA = BnA / N 
        P_BnC = BnC / N 
        P_BnD = BnD / N
        P_CnA = CnA / N 
        P_CnB = CnB / N 
        P_CnD = CnD / N
        P_DnA = DnA / N 
        P_DnB = DnB / N 
        P_DnC = DnC / N 

        """
        Cálculo de las probabilidades condicionales.
        """
        P_B_A = P_BnA / PA
        P_C_A = P_CnA / PA  
        P_D_A = P_DnA / PA 
        P_A_B = P_AnB / PB  
        P_C_B = P_CnB / PB  
        P_D_B = P_DnB / PB 
        P_A_C = P_AnC / PC  
        P_B_C = P_BnC / PC  
        P_D_C = P_DnC / PC 
        P_A_D = P_AnD / PD  
        P_B_D = P_BnD / PD  
        P_C_D = P_CnD / PD  

        # Se imprimen las probabilidades condicionales.
        print ( "\n\tP(B|A): ", P_B_A )
        print ( "\tP(C|A): ", P_C_A ) 
        print ( "\tP(D|A): ", P_D_A )
        print ( "\tP(A|B): ", P_A_B ) 
        print ( "\tP(C|B): ", P_C_B ) 
        print ( "\tP(D|B): ", P_D_B )
        print ( "\tP(A|C): ", P_A_C ) 
        print ( "\tP(B|C): ", P_B_C ) 
        print ( "\tP(D|C): ", P_D_C )
        print ( "\tP(A|D): ", P_A_D ) 
        print ( "\tP(B|D): ", P_B_D ) 
        print ( "\tP(C|D): ", P_C_D ) 



        """
        Parte 3: Relaciones de dependencia o independencia
        """

        print ("\n3. ¿Hay relaciones de dependencia o independencia entre las características? ¿De qué tipo?")

        """
        Se llama a la función "relacion_dep_indep" para cada par de características que se desean analizar, esta función
        imprime en pantalla si esas características son dependientes o independientes. Note que se utilizó un umbral de 10% 
        en caso de que las probabilidades calculadas sean cercanas a la probabilidad conjunta pero no iguales.
        """
        relacion_dep_indep (P_AnB, PA, PB, 'A', 'B')

        relacion_dep_indep (P_AnC, PA, PC, 'A', 'C')       

        relacion_dep_indep (P_AnD, PA, PD, 'A', 'D')
        
        relacion_dep_indep (P_BnC, PB, PC, 'B', 'C')
        
        relacion_dep_indep (P_BnD, PB, PD, 'B', 'D')
        
        relacion_dep_indep (P_CnD, PC, PD, 'C', 'D')
        


        """
        Parte 4: Si hay una característica tipo D, ¿cuál es la probabilidad 
        de que también tenga la característica A?
        """

        print ("\n4. Si hay una característica tipo D, ¿cuál es la probabilidad de que también tenga la característica A?")

        """
        Se imprime el resultado de aplicar el teorema de Bayes, este resultado se calcula en la función "Bayes".
        """
        print ("\nUsando Bayes:")
        print ( "\n\tP(A|D): ", Bayes ( P_D_A, PA, PD ) )


    print("\n-> Fin del programa")