# Tarea 1

## Integrante
```
Jorge Muñoz Taylor 
A53863
```

## Curso
```
Modelos probabilísticos de señales y sistemas
```

## COMO EJECUTAR EL CÓDIGO

Una vez descargado el código, ubíquese en la carpeta donde está el código:
```
>>python3 A53863.py lote.csv
```

## IMPORTANTE
```
El código fue creado para ejecutarse en PYTHON 3, en otras versiones de python puede no funcionar,
también tome en cuenta que el programa se probó en sistemas operativos Linux (Ubuntu).
```
08/05/2020